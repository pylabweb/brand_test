from django.core.management.base import BaseCommand
from django.utils.timezone import datetime
from home.models import LogData
from tqdm import tqdm
import requests
import re

class Command(BaseCommand):
    # python manage.py import_logs http://www.almhuette-raith.at/apache-log/access.log
    err_list = [] # строки лога не попавшие в БД
    help = 'Import data command'

    def add_arguments(self, parser):
        parser.add_argument('URL', type=str)

    def handle(self, *args, **options):
        # Загрузка файла по ссылке
        self.stdout.write("Загрузка данных:")
        url = options['URL']
        testread = requests.head(url)
        filelength = int(testread.headers['Content-length'])
        r = requests.get(url, stream=True)
        pbar = tqdm(total=int(filelength / 1048576))
        for chunk in r.iter_content(chunk_size=1048576):
            if chunk:
                pbar.update()
                text=chunk.decode("utf-8").split('\n')
                self.import_in_db(text)
        pbar.close()
        if len(self.err_list) > 0:
            thefile = open('errors.txt', 'w')
            for item in self.err_list:
                thefile.write("%s\n" % item)
        self.stdout.write("Загрузка данных в БД завершена!")


        # Загрузка данных в БД из файла log.txt
    def import_in_db (self,text):
        ip_pattern = r'^\d+\.\d+\.\d+\.\d+'
        http_method_pattern = r'(?:OPTIONS|GET|HEAD|POST|PUT|PATCH|DELETE|TRACE|CONNECT)'
        date_time_pattern = r'\d+/\w+/\d+:\d+:\d+:\d+'
        url_request_pattern = r'(]\s\S+\s)(\S+)'
        response_code_pattern = r'(?<="\s)\d+'
        response_size_pattern = '(?<="\s\d\d\d\s)\d+'
        log_list=[]
        for str in text:
            try:
                ip = re.search(ip_pattern, str).group(0)
                http_method = re.search(http_method_pattern, str)
                if http_method is not None:
                    http_method=http_method.group(0)
                else:
                    http_method=''
                date_time=re.search(date_time_pattern, str).group(0)
                date_time = datetime.strptime(date_time, "%d/%b/%Y:%H:%M:%S")
                url_request=re.search(url_request_pattern, str).group(2)
                response_code = re.search(response_code_pattern, str).group(0)
                response_code=int(response_code)
                response_size = re.search(response_size_pattern, str)
                if response_size is None:
                    response_size = 0
                else:
                    response_size = int(response_size.group(0))
                log_list.append(LogData(ip=ip,
                                        date_time=date_time,
                                        http_method = http_method,
                                        url_request=url_request,
                                        response_code=response_code,
                                        response_size=response_size,))
            except:
                if len(str) > 0:
                    self.err_list.append(str)
        LogData.objects.bulk_create(log_list)

