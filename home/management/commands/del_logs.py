from django.core.management.base import BaseCommand
from home.models import LogData

class Command(BaseCommand):
    # python manage.py del_logs
    def handle(self, *args, **options):
        LogData.objects.all().delete()