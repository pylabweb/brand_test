from django.shortcuts import render
from django.db.models import Sum,Count,Q
from django.db import connection
from django.core.paginator import Paginator
from django.core import management
from django.template.defaulttags import register
from .models import LogData

cursor = connection.cursor()

@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)

def log_list(request):
    logs = LogData.objects.get_queryset().order_by('id')

    #Удалить данные
    if request.GET.get('delete'):
        LogData.objects.all().delete()

    #Загрузить данные в БД
    if request.GET.get('loaddata'):
        management.call_command('import_logs','http://www.almhuette-raith.at/apache-log/access.log')

    #Поиск
    query=request.GET.get('q')
    if query:
        logs=logs.filter(
            Q(ip__contains=query)|
            Q(date_time__contains=query) |
            Q(http_method__contains=query) |
            Q(url_request__contains=query) |
            Q(response_code__contains=query) |
            Q(response_size__contains=query)
        )

    # Статистика
    # Сумма байт
    response_size_sum = logs.aggregate(Sum('response_size'))['response_size__sum']

    # Уникальные IP
    count_ip=logs.aggregate(Count('ip',distinct=True))['ip__count']

    # Количество GET, POST
    http_methods=logs.values('http_method').annotate(frequency=Count('http_method')).order_by('-frequency')

    # 10 наиболее частых IP
    top_ip=logs.values('ip').annotate(frequency=Count('ip')).order_by('-frequency')[:10]


    #Пагинация
    paginator = Paginator(logs, 50)
    page = request.GET.get('page')
    logs = paginator.get_page(page)

    content={'logs': logs,
             'response_size_sum':response_size_sum,
             'count_ip':count_ip,
             'top_ip':top_ip,
             'http_methods':http_methods,
             }
    return render(request, 'home/log_list.html', content)
