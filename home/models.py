from django.db import models

class LogData(models.Model):
    ip = models.GenericIPAddressField('IP - адрес')
    date_time = models.DateTimeField('Дата и время')
    http_method = models.CharField('HTTP - метод',max_length=10)
    url_request=models.CharField('URL страницы',max_length=20000)
    response_code=models.PositiveSmallIntegerField('Код ответа')
    response_size=models.PositiveIntegerField('Размер ответа (байт)')

    class Meta:
        db_table='logdata'
        verbose_name = 'Запись log - файла'
        verbose_name_plural = 'Записи log - файла'

